##Importing the libraries and Dependencies
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import csv
from pandas import read_csv
import os

##File reading using pandas
data = pd.read_csv("beach-weather-stations-automated-sensors-1.csv")                                                   

##Calculation of the number of missing values in the dataset
print("Number of null values in each column")                                   
print(data.isnull().sum())                                                                                              

##Inserting NA in the missing value space.
data.fillna("NA", inplace=True)                                                                                        

##Conversion of the data type of the columns from string to numeric and filling in all the NA values with 0 or the mean value of that the column. Conversion of certain columns to integer data type as well.
print(data)
print(data.dtypes)

data["Measurement Record Id"] = pd.to_numeric(data["Measurement Record Id"], errors='coerce')                              
data["Measurement Record Id"] = data["Measurement Record Id"].fillna(0, downcast='infer')

data["Wet Bulb Temperature"] = pd.to_numeric(data["Wet Bulb Temperature"], errors='coerce')
data["Wet Bulb Temperature"] = data["Wet Bulb Temperature"].fillna(data["Wet Bulb Temperature"].mean())

data["Humidity"] = pd.to_numeric(data["Humidity"], errors='coerce')
data["Humidity"] = data["Humidity"].fillna(data["Humidity"].mean(), downcast='infer').astype(np.int64)

data["Rain Intensity"] = pd.to_numeric(data["Rain Intensity"], errors='coerce')
data["Rain Intensity"] = data["Rain Intensity"].fillna(data["Rain Intensity"].mean())

data["Total Rain"] = pd.to_numeric(data["Total Rain"], errors='coerce')
data["Total Rain"] = data["Total Rain"].fillna(data["Total Rain"].mean())

data["Air Temperature"] = pd.to_numeric(data["Air Temperature"], errors='coerce')
data["Air Temperature"] = data["Air Temperature"].fillna(data["Air Temperature"].mean())

data["Wind Speed"] = pd.to_numeric(data["Wind Speed"], errors='coerce')
data["Wind Speed"] = data["Wind Speed"].fillna(data["Wind Speed"].mean())

data["Solar Radiation"] = pd.to_numeric(data["Solar Radiation"], errors='coerce')
data["Solar Radiation"] = data["Solar Radiation"].fillna(data["Solar Radiation"].mean(), downcast='infer').astype(np.int64)

##Printing the data types of the column to verify if the data type has been converted successfully.
print(data.dtypes)
print(data)

##Check to verify if all the missing values have been filled up.
print("After replacing null values with the mean value of each column")                 
print(data.isnull().sum())

##Printing of the statistics of the data set
summary = data.describe()       
print("Summary Statistics")
print(summary)

##Redirecting the clean data set to a csv file.
data.to_csv('Cleaned_dataset.csv', index=False, encoding='utf-8')    

####Humidity vs Rain Intensity Scatter plot
plt.scatter(data['Humidity'],data['Rain Intensity'], color='green', marker='*')                                          
plt.title('Humidity versus Rain Intensity', color='red')
plt.xlabel('Humidity', color='blue')
plt.ylabel('Rain Intensity', color='blue')
plt.show()

##Total Rain and Solar Radiation Box plot
df = pd.DataFrame(data, columns=['Total Rain','Solar Radiation'])
df.plot.box(grid='True')
plt.show()

####Solar Radiation vs Rain Intensity Scatter plot
plt.scatter(data['Solar Radiation'],data['Wet Bulb Temperature'], color='green', marker='*')                                          
plt.title('Solar Radiation versus Wet Bulb Temperature', color='red')
plt.xlim(0,200)
plt.ylim(0,200)
plt.xlabel('Solar Radiation', color='blue')
plt.ylabel('Wet Bulb Temperature', color='blue')
plt.show()
